const GraknClient = require("grakn-client");
const fs = require("fs");
const { parser } = require("stream-json");
const { streamArray } = require("stream-json/streamers/StreamArray");
const { chain } = require("stream-chain");

const inputs = [
  { dataPath: "./files/phone_calls/data/companies", template: companyTemplate },
  { dataPath: "./files/phone_calls/data/people", template: personTemplate },
  { dataPath: "./files/phone_calls/data/contracts", template: contractTemplate },
  { dataPath: "./files/phone_calls/data/calls", template: callTemplate },
];

/**
 * gets the job done:
 * 1. creates a Grakn instance
 * 2. creates a session to the targeted keyspace
 * 3. loads json to Grakn for each file
 * 4. closes the session
 * 5. closes the client
 */

async function buildPhoneCallGraph(inputs) {
  const client = new GraknClient("localhost:48555");
  const session = await client.session("phone_calls");

  for (input of inputs) {
    await loadDataIntoGrakn(input, session);
  }
  await session.close();
  client.close();
}

/**
 * loads the json data into our Grakn phone_calls keyspace
 * @param {object} input contains details required to parse the data
 * @param {object} session a Grakn session, off of which a transaction will be created
 */
async function loadDataIntoGrakn(input, session) {
  const items = await parseDataToObjects(input);

  for (item of items) {
    const transaction = await session.transaction().write();
    const graqlInsertQuery = input.template(item);
    await transaction.query(graqlInsertQuery);
    await transaction.commit();
  }
}

/**
 * 1. reads the file through a stream,
 * 2. adds the  object to the list of items
 * @param {string} input.dataPath path to the data file
 * @returns items that is, a list of objects each representing a data item
 */
function parseDataToObjects(input) {
  const items = [];
  return new Promise(function (resolve, reject) {
    const pipeline = chain([
      fs.createReadStream(input.dataPath + ".json"),
      parser(),
      streamArray()
    ]);

    pipeline.on("data", function (result) {
      items.push(result.value);
    });

    pipeline.on("end", function () {
      resolve(items);
    });
  });
}

// **********************
// The Template Functions
// **********************

function companyTemplate(company) {
  return `insert $company isa company, has name "${company.name}";`;
}

function personTemplate(person) {
  const { first_name, last_name, phone_number, city, age } = person;

  // insert person
  let graqlInsertQuery = `insert $person isa person, has phone-number "${phone_number}"`;
  const isNotCustomer = typeof first_name === "undefined";

  if (isNotCustomer) {
    // person is not a customer
    graqlInsertQuery += ", has is-customer false";
  } else {
    // person is a customer
    graqlInsertQuery += `, has is-customer true`;
    graqlInsertQuery += `, has first-name "${first_name}"`;
    graqlInsertQuery += `, has last-name "${last_name}"`;
    graqlInsertQuery += `, has city "${city}"`;
    graqlInsertQuery += `, has age ${age}`;
  }

  graqlInsertQuery += ";";
  return graqlInsertQuery;
}

function contractTemplate(contract) {
  const { company_name, person_id } = contract;
  // match company
  let graqlInsertQuery = `match $company isa company, has name "${company_name}"; `;
  // match person
  graqlInsertQuery += `$customer isa person, has phone-number "${person_id}"; `;
  // insert contract
  graqlInsertQuery +=
    "insert (provider: $company, customer: $customer) isa contract;";
  return graqlInsertQuery;
}

function callTemplate(call) {
  const { caller_id, callee_id, started_at, duration } = call;

  // match caller
  let graqlInsertQuery = `match $caller isa person, has phone-number "${caller_id}"; `;

  // match callee
  graqlInsertQuery += `$callee isa person, has phone-number "${callee_id}"; `;

  // insert call
  graqlInsertQuery += `insert $call(caller: $caller, callee: $callee) isa call; $call has started-at ${started_at}; $call has duration ${duration};`;
  return graqlInsertQuery;
}

buildPhoneCallGraph(inputs);
